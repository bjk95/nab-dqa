import org.apache.spark.rdd.RDD
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.types.{ArrayType, DataType, IntegerType, LongType, StringType, StructField, StructType}
import org.apache.spark.sql.{Column, DataFrame, Dataset, Row, SparkSession, TypedColumn}
import org.apache.spark.sql.functions._

object DataQualityAnalysis extends App {
  val spark = SparkSession.builder().master("local[*]").getOrCreate()
  import spark.implicits._


  // Reading data
  val path = "src/main/resources/userdata1.parquet"
//  val rawDF = spark.read.parquet(path)

  val b = B("hello", 8)
  val a = A(b)
  val c =  12

  val rawDF = Seq((a, c),(a, c),(a, c),(a, c)).toDF()


  // Functions
  def applyUdfToEachColumn(df: DataFrame, udfs: Map[DataType, UserDefinedFunction], schema: Seq[StructField]): DataFrame = {
    def loop(df: DataFrame, udfs: Map[DataType, UserDefinedFunction], schema: Seq[StructField], schemaPath: List[String]): DataFrame = {
      schema.foldLeft(df) { (accumulatorDF: DataFrame, column: StructField) =>
        column.dataType match {
          case StructType(s) =>

            loop(accumulatorDF, udfs, s, schemaPath :+ column.name)

          case _ => applyUdfToCol(accumulatorDF, column, udfs, schemaPath)
        }
      }
    }
      loop(df, udfs, schema, List.empty)
  }

      def applyUdfToCol(df: DataFrame, column: StructField, udfs: Map[DataType, UserDefinedFunction], path: List[String]): DataFrame = {
        val actualPath = (path :+ column.name).mkString(".")
        val udf: Option[UserDefinedFunction] = udfs.get(column.dataType)
        udf match {
          case Some(function) => df.withColumn(actualPath, function(col(actualPath )))
          case _ => df
        }

      }




  // Print sample of data
  rawDF.show()


  // Print count, mean, stddev, min and max
  val schema = rawDF.schema
  schema.printTreeString()

  val rowCount = rawDF.count()
  val summaryStats: DataFrame = rawDF.summary().persist()
  summaryStats.show()




  // Count of nulls per column

  val counts: DataFrame = summaryStats.where($"summary" === "count")
  val subtractCount = (c: Long) => rowCount - c
  val subtractCountUDF = udf(subtractCount)
  val nullCountDF = applyUdfToEachColumn(counts, Map(LongType -> subtractCountUDF), summaryStats.schema)

  nullCountDF.show()
  summaryStats.unpersist()


  // String functions
  val stringColumnSchema: Seq[StructField] = rawDF.schema.filter(_.dataType match {
    case StringType => true
    case _ => false
  })

  val toStringLength = (s: String) => s.length
  val toStringLengthUDF: UserDefinedFunction = udf((s: String) => s.length)
  val times2Udf = udf((i: Int) => i * 2)

  val tempDf = applyUdfToEachColumn(rawDF, Map(StringType -> toStringLengthUDF, IntegerType -> times2Udf), rawDF.schema)
  tempDf.show()



}

case class A(
              b: B
            )

case class B(
              string: String,
            s2: Int
            )
